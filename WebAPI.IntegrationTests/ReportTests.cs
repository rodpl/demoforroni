using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using NUnit.Framework;
using WebAPI.Models;

namespace WebAPI.IntegrationTests
{
    public class ReportTests
    {
        [Test]
        public async Task GetRoomTypesWithStatusesSimpleTest()
        {
            // Arrange
            var host = await CreateHostAsync();
            var client = host.GetTestClient();

            // Act
            var response = await client.GetAsync("/reports/getRoomTypesWithStatuses");

            // Assert
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            var jsonOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var result = JsonSerializer.Deserialize<RoomTypesWithStatusDto[]>(content, jsonOptions);
            var room1b = result.Single(x => x.RoomTypeName == "1b");
            Assert.AreEqual(18, room1b.Statuses.Single(x => x.StatusName == "Complete").Count);
            Assert.AreEqual(10, room1b.Statuses.Single(x => x.StatusName == "Delayed").Count);


        }

        private static async Task<IHost> CreateHostAsync()
        {
            var hostBuilder = new HostBuilder()
                .ConfigureWebHost(webHost =>
                {
                    webHost.ConfigureAppConfiguration((context, builder) =>
                    {
                        builder.AddJsonFile("appsettings.Test.json");

                    });
                    webHost.UseTestServer();
                    webHost.UseEnvironment("Development");
                    webHost.UseStartup<WebAPI.Startup>();
                });

            return await hostBuilder.StartAsync();
        }
    }
}