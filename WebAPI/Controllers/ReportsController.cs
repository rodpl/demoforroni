﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ReportsController : ControllerBase
    {
        private readonly ReportFinder reportFinder;

        public ReportsController(ReportFinder reportFinder)
        {
            this.reportFinder = reportFinder;
        }

        [HttpGet]
        public async Task<IEnumerable<RoomTypesWithStatusDto>> GetRoomTypesWithStatuses()
        {
            return await this.reportFinder.RoomTypesWithStatusesAsync();
        }
    }
}