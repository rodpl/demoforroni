﻿namespace WebAPI.Models
{
    public class RoomTypesWithStatusDto
    {
        public class StatusSummary
        {
            public string StatusName { get; set; }
            public int Count { get; set; }
        }

        public string  RoomTypeName { get; set; }

        public StatusSummary[] Statuses { get; set; }
    }
}