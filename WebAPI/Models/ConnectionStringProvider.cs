﻿using Microsoft.Extensions.Configuration;

namespace WebAPI.Models
{
    public class ConnectionStringProvider
    {
        private readonly IConfiguration configuration;

        public ConnectionStringProvider(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public string GetConnectionString()
        {
            return this.configuration.GetConnectionString("DefaultConnection");
        }
    }
}