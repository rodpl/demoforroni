﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace WebAPI.Models
{
    public class ReportFinder
    {
        private readonly ConnectionStringProvider connectionStringProvider;

        public ReportFinder(ConnectionStringProvider connectionStringProvider)
        {
            this.connectionStringProvider = connectionStringProvider;
        }

        public async Task<IEnumerable<RoomTypesWithStatusDto>> RoomTypesWithStatusesAsync()
        {
            const string query = @"
SELECT COUNT(*) AS Count, rt.Name AS RoomType, j.Status AS JobStatus
FROM RX_RoomType AS rt
INNER JOIN RX_Job AS j ON (rt.Id) = j.RoomTypeId
GROUP BY rt.Name, j.[Status]";

            using (var conn = this.CreateConnection())
            {
                await conn.OpenAsync();

                using (var transaction = await this.CreateTransactionForQueries(conn))
                {
                    var dbResult = await conn.QueryAsync<RoomTypeWithStatusRow>(query, transaction: transaction);
                    var result = dbResult
                        .GroupBy(x => x.RoomType, x => new RoomTypesWithStatusDto.StatusSummary()
                        {
                            StatusName = x.JobStatus,
                            Count = x.Count
                        })
                        .Select(gr =>
                            new RoomTypesWithStatusDto()
                            {
                                RoomTypeName = gr.Key,
                                Statuses = gr.ToArray()
                            });
                    return result;
                }
            }
        }

        /// <summary>
        /// Queries should be run as ReadUncommitted to avoid unnecessary locking
        /// </summary>
        private async Task<IDbTransaction> CreateTransactionForQueries(SqlConnection conn)
        {
            return await conn.BeginTransactionAsync(IsolationLevel.ReadUncommitted);
        }

        private SqlConnection CreateConnection()
        {
            return new SqlConnection(this.connectionStringProvider.GetConnectionString());
        }

        private class RoomTypeWithStatusRow
        {
            public string RoomType { get; set; }
            public string JobStatus { get; set; }
            public int Count { get; set; }
        }
    }
}
